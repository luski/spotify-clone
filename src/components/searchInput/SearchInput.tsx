import React, {ChangeEvent} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

import './searchInput.scss';

type Props = {
  onChange: (event: ChangeEvent<HTMLInputElement>) => void;
  value: string;
};

const SearchInput = (props: Props) => {
  return (
    <div className="search-input-wrapper">
      <FontAwesomeIcon icon="search" size="lg" />
      <input {...props} type="text" className="search-input" placeholder="Search" />
    </div>
  );
};

export default SearchInput;
