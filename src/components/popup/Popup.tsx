import React, {useRef, useState} from 'react';
import cx from 'classnames';
import useOutsideClick from 'react-outside-click-hook';

import './popup.scss';

type Props = {
  items: Array<{label: string; onClick: () => void}>;
  handlerContent: React.FunctionComponent<{open: boolean}>;
};

const Popup = ({handlerContent, items}: Props) => {
  const [open, setOpen] = useState(false);
  const HandlerContent: React.FunctionComponent<{open: boolean}> = handlerContent;

  const listRef = useRef(null);
  const handlerRef = useRef(null);
  useOutsideClick([listRef, handlerRef], (isInside: boolean) => {
    if (!isInside) setOpen(false);
  });

  return (
    <div className="profile-button-wrapper">
      <button className="handler-button" onClick={() => setOpen(open => !open)} ref={handlerRef}>
        <HandlerContent open={open} />
      </button>
      <ul className={cx('menu', {open})} ref={listRef}>
        {items.map(({label, onClick}) => (
          <li key={label}>
            <button onClick={onClick} className="txt bold">{label}</button>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Popup;
