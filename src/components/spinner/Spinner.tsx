import React from 'react';
import ClipLoader from 'react-spinners/ClipLoader';

import './spinner.scss';

export default function Spinner() {
  return (
    <div className="spinner-wrapper">
      <ClipLoader size={150} color="#38DA61" loading={true} />
    </div>
  );
}
