import React, {FunctionComponent, useCallback, useEffect, useState} from 'react';

import AppContext from './AppContext';
import useAuth from './hooks/useAuth';
import {fetchCategories, fetchUser, setAccessToken} from '../api/api';
import usePlayer from './hooks/usePlayer';
import {Category, User} from '../api/types';

const AppContextProvider: FunctionComponent = ({children}) => {
  const [me, setMe] = useState<User | null>(null);
  const [initializing, setInitializing] = useState<boolean>(false);
  const [loggedIn, setLoggedIn] = useState(false);
  const [token, login, logout] = useAuth();
  const [categories, setCategories] = useState<Category[]>([]);
  const player = usePlayer();

  const onLogout = useCallback(() => {
    logout();
    setLoggedIn(false);
  }, [logout]);

  useEffect(() => {
    if (token) {
      setInitializing(true);
      setAccessToken(token);
      fetchUser().then(user => {
        setMe(user);
        setLoggedIn(true);
        setInitializing(false);
      });
      fetchCategories().then(setCategories);
    }
  }, [token]);

  return (
    <AppContext.Provider value={{login, logout: onLogout, loggedIn, me, categories, initializing, player}}>
      {children}
    </AppContext.Provider>
  );
};

export default AppContextProvider;
