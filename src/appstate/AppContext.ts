import {createContext} from 'react';
import {Player} from './hooks/usePlayer';
import {Category, User} from '../api/types';

type AppContextType = {
  initializing: boolean;
  loggedIn: boolean;
  me: User | null;
  login: () => any;
  logout: () => any;
  categories: Category[];
  player: Player;
};

export default createContext<AppContextType>({
  initializing: false,
  loggedIn: false,
  me: null,
  login: () => {},
  logout: () => {},
  categories: [],
  player: {
    play: () => {},
    pause: () => {},
    nextTrack: () => {},
    previousTrack: () => {},
    playAnother: () => {},
    playing: false,
    currentUrl: '',
    elapsedTime: 0,
    getDuration: () => 1,
    setElapsedTime: () => {},
    playlist: null,
    currentTrack: undefined
  }
});
