import {useCallback, useState} from 'react';

import loginToSpotify from '../../api/login';
import {getItem, setItem} from '../../utils/storage';

const KEY = 'spotify-clone-access-token';

function saveAccessToken(token: string, expirationTime: number) {
  setItem(KEY, JSON.stringify({token, expirationTime}));
}

function getAccessToken() {
  const item = getItem(KEY);
  if (!item) {
    return null;
  }
  const object = JSON.parse(item);
  if (object.token && object.expirationTime && object.expirationTime > Date.now()) {
    return object['token'];
  }
  return null;
}

type ReturnType = [string | null, () => any, () => any];

export default function useAuth(): ReturnType {
  const [token, setToken] = useState<string | null>(getAccessToken());
  const login = useCallback(async () => {
    const {token, expiresIn} = await loginToSpotify();
    setToken(token);
    saveAccessToken(token, Date.now() + expiresIn * 1000);
  }, []);

  const logout = useCallback(() => {
    setToken(null);
    saveAccessToken('', 0);
  }, []);

  return [token, login, logout];
}
