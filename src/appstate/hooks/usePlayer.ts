import {Reducer, useEffect, useMemo, useReducer, useRef} from 'react';
import {throttle} from 'lodash';

import {Playlist, PlaylistTrack} from '../../api/types';
import {getItem, setItem} from '../../utils/storage';

const KEY = 'spotify-clone-player-state';

type PlayerState = {
  url: string | null;
  elapsedTime: number;
  tracks: PlaylistTrack[];
  playlist: Playlist | null;
};

export type Player = {
  play: () => void;
  pause: () => void;
  playAnother: (url: string, tracks: PlaylistTrack[], playlist: Playlist | null) => void;
  nextTrack: () => void;
  previousTrack: () => void;
  playing: boolean;
  currentUrl: string;
  elapsedTime: number;
  getDuration: () => number;
  setElapsedTime: (elapsedTime: number) => void;
  playlist: Playlist | null;
  currentTrack: PlaylistTrack | undefined;
};

function restorePlayerState(): PlayerState {
  const json = getItem(KEY);
  if (json) {
    return JSON.parse(json) as PlayerState;
  }
  return {url: '', elapsedTime: 0, tracks: [], playlist: null};
}

const storePlayerState = throttle(
  (url: string | null, elapsedTime: number, tracks: PlaylistTrack[], playlist: Playlist | null) => {
    setItem(KEY, JSON.stringify({url, elapsedTime, tracks, playlist}));
  },
  2000
);

enum ActionType {
  initialize,
  play,
  pause,
  playAnother,
  nextTrack,
  previousTrack,
  refreshElapsedTime,
  setElapsedTime,
  ready
}

type Action =
  | {type: ActionType.initialize}
  | {type: ActionType.play}
  | {type: ActionType.pause}
  | {type: ActionType.playAnother; payload: {url: string; tracks: PlaylistTrack[]; playlist: Playlist | null}}
  | {type: ActionType.nextTrack}
  | {type: ActionType.previousTrack}
  | {type: ActionType.refreshElapsedTime; payload: {elapsedTime: number}}
  | {type: ActionType.setElapsedTime; payload: {elapsedTime: number}}
  | {type: ActionType.ready};

type State = {
  action: ActionType;
  trackUrl: string;
  tracks: PlaylistTrack[];
  playlist: Playlist | null;
  playing: boolean;
  elapsedTime: number;
};

function getNextTrack(url: string, tracks: PlaylistTrack[]): PlaylistTrack | null {
  const currentIndex = tracks.findIndex(track => track.url === url);
  if (currentIndex !== -1 && tracks.length > currentIndex + 1) {
    return tracks[currentIndex + 1];
  }
  return null;
}

function getPreviousTrack(url: string, tracks: PlaylistTrack[]): PlaylistTrack | null {
  const currentIndex = tracks.findIndex(track => track.url === url);
  if (currentIndex !== -1 && tracks.length > 0) {
    return tracks[Math.max(0, currentIndex - 1)];
  }
  return null;
}

const reducer: Reducer<State, Action> = (state: State, action: Action) => {
  switch (action.type) {
    case ActionType.play:
      return {...state, action: action.type, playing: true};

    case ActionType.pause:
      return {...state, action: action.type, playing: false};

    case ActionType.playAnother:
      return {
        ...state,
        action: action.type,
        trackUrl: action.payload.url,
        playlist: action.payload.playlist,
        elapsedTime: 0,
        tracks: action.payload.tracks.filter(({url}) => !!url)
      };

    case ActionType.refreshElapsedTime:
    case ActionType.setElapsedTime:
      return {...state, action: action.type, elapsedTime: action.payload.elapsedTime};

    case ActionType.nextTrack: {
      const nextTrack = getNextTrack(state.trackUrl, state.tracks);
      return {
        ...state,
        action: action.type,
        elapsedTime: 0,
        trackUrl: nextTrack ? nextTrack.url : ''
      };
    }

    case ActionType.previousTrack: {
      const previousTrack = getPreviousTrack(state.trackUrl, state.tracks);
      return {
        ...state,
        action: action.type,
        elapsedTime: 0,
        trackUrl: previousTrack ? previousTrack.url : ''
      };
    }

    case ActionType.initialize:
    case ActionType.ready:
      return {...state, action: action.type};
  }
};

export default function usePlayer(): Player {
  const initialPlayerState = restorePlayerState();
  const [{playing, elapsedTime, trackUrl, action, tracks, playlist}, dispatch] = useReducer<Reducer<State, Action>>(
    reducer,
    {
      trackUrl: initialPlayerState.url || '',
      tracks: initialPlayerState.tracks || [],
      elapsedTime: initialPlayerState.elapsedTime || 0,
      playing: false,
      playlist: initialPlayerState.playlist,
      action: ActionType.initialize
    }
  );

  const currentTrack = useMemo<PlaylistTrack | undefined>(() => tracks.find(({url}) => url === trackUrl), [
    tracks,
    trackUrl
  ]);

  useEffect(() => {
    if (playing) {
      storePlayerState(trackUrl, elapsedTime, tracks, playlist);
    }
  }, [trackUrl, elapsedTime, playing, tracks, playlist]);

  const audioRef = useRef(new Audio());

  useEffect(() => {
    const audio = audioRef.current;
    if (playing) {
      const intervalId = setInterval(() => {
        dispatch({type: ActionType.refreshElapsedTime, payload: {elapsedTime: audio.currentTime}});
      }, 200);

      return () => clearInterval(intervalId);
    }
  }, [playing]);

  useEffect(() => {
    const audio = audioRef.current;
    const listener = () => dispatch({type: ActionType.nextTrack});
    audio.addEventListener('ended', listener);
    return () => audio.removeEventListener('ended', listener);
  }, []);

  useEffect(() => {
    const audio = audioRef.current;
    if (audio) {
      switch (action) {
        case ActionType.initialize:
          audio.src = trackUrl;
          audio.currentTime = elapsedTime;
          setTimeout(() => audio.pause(), 0);
          dispatch({type: ActionType.ready});
          break;

        case ActionType.play:
          audio.play();
          dispatch({type: ActionType.ready});
          break;

        case ActionType.pause:
          audio.pause();
          dispatch({type: ActionType.ready});
          break;

        case ActionType.playAnother:
        case ActionType.nextTrack:
        case ActionType.previousTrack:
          audio.pause();
          audio.src = trackUrl;
          if (trackUrl.length) dispatch({type: ActionType.play});
          break;

        case ActionType.setElapsedTime:
          audio.currentTime = elapsedTime;
          dispatch({type: ActionType.ready});
          break;
      }
    }
  }, [action, audioRef, elapsedTime, trackUrl]);

  return {
    play: () => dispatch({type: ActionType.play}),
    pause: () => dispatch({type: ActionType.pause}),
    playAnother: (url: string, tracks: PlaylistTrack[], playlist: Playlist | null) =>
      dispatch({type: ActionType.playAnother, payload: {url, tracks, playlist}}),
    currentUrl: trackUrl,
    playing,
    elapsedTime,
    playlist,
    currentTrack,
    nextTrack: () => dispatch({type: ActionType.nextTrack}),
    previousTrack: () => dispatch({type: ActionType.previousTrack}),
    getDuration: () => audioRef.current.duration,
    setElapsedTime: (elapsedTime: number) => dispatch({type: ActionType.setElapsedTime, payload: {elapsedTime}})
  };
}
