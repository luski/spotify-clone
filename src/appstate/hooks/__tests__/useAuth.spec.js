import React from 'react';
import {renderHook, act} from '@testing-library/react-hooks';
import useAuth from '../useAuth';

import * as storage from '../../../utils/storage';
import loginMock from '../../../api/login';

jest.mock('../../../utils/storage');
jest.mock('../../../api/login');

const wrapper = ({children}) => <div>{children}</div>;

describe('useAuth hook', () => {
  it('should return token equal to null initially and login and logout functions', () => {
    const {
      result: {
        current: [token, login, logout]
      }
    } = renderHook(() => useAuth(), {wrapper});
    expect(token).toEqual(null);
    expect(login).toBeInstanceOf(Function);
    expect(logout).toBeInstanceOf(Function);
  });

  it('should return token equal to null if token is stored in localStorage, but its expiration time passed', () => {
    storage.getItem.mockImplementation(() => JSON.stringify({token: 'my-token', expirationTime: Date.now() - 10000}));
    const {
      result: {
        current: [token]
      }
    } = renderHook(() => useAuth(), {wrapper});
    expect(token).toEqual(null);
  });

  it('should return token stored in localStorage if possible', () => {
    storage.getItem.mockImplementation(() => JSON.stringify({token: 'my-token', expirationTime: Date.now() + 100000}));
    const {
      result: {
        current: [token]
      }
    } = renderHook(() => useAuth(), {wrapper});
    expect(token).toEqual('my-token');
  });

  describe('useAuth - login function', () => {
    it('should perform login to spotify and after that update the auth token and write it to local storage', async () => {
      storage.getItem.mockImplementation(() => null);
      loginMock.mockImplementation(() =>
        Promise.resolve({
          token: 'my-new-token',
          expiresIn: 3600
        })
      );
      const now = new Date('2020-01-01').getTime();
      jest.spyOn(Date, 'now').mockImplementation(() => now);
      const {result, waitForNextUpdate} = renderHook(() => useAuth(), {wrapper});
      const login = result.current[1];
      let token = result.current[0];
      expect(token).toBeNull();
      login();
      await waitForNextUpdate();
      token = result.current[0];
      expect(token).toEqual('my-new-token');
      expect(storage.setItem).toHaveBeenCalledWith(
        'spotify-clone-access-token',
        JSON.stringify({token: 'my-new-token', expirationTime: now + 3600 * 1000})
      );
    });
  });
  describe('useAuth - logout function', () => {
    it('should set auth token to null', async () => {
      storage.getItem.mockImplementation(() =>
        JSON.stringify({token: 'my-token', expirationTime: Date.now() + 100000})
      );
      const {result} = renderHook(() => useAuth(), {wrapper});
      let token = result.current[0];
      expect(token).toEqual('my-token');
      const logout = result.current[2];
      act(() => {
        logout();
      });
      token = result.current[0];
      expect(token).toBeNull();
      expect(storage.setItem).toHaveBeenCalledWith(
        'spotify-clone-access-token',
        JSON.stringify({token: '', expirationTime: 0})
      );
    });
  });
});
