import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';

import AppUI from './pages/AppUI';
import SignInCallbackPage from './pages/SignInCallbackPage';
import AppContextProvider from './appstate/AppContextProvider';
import {library} from '@fortawesome/fontawesome-svg-core';
import {fas} from '@fortawesome/free-solid-svg-icons';

import 'typeface-roboto';
import './styles/layout.scss';

library.add(fas);

function App() {
  return (
    <AppContextProvider>
      <BrowserRouter>
        <Switch>
          <Route path="/signin-callback">
            <SignInCallbackPage />
          </Route>
          <Route path="/">
            <AppUI />
          </Route>
        </Switch>
      </BrowserRouter>
    </AppContextProvider>
  );
}

export default App;
