export function formatDuration(millis: number) {
  const [hours, minutes, seconds] = new Date(millis)
    .toISOString()
    .slice(11, -5)
    .split(':');
  if (hours === '00') {
    return `${Number(minutes)}:${seconds}`;
  }

  return `${Number(hours)}:${minutes}:${seconds}`;
}
