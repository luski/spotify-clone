import React, {useContext, useEffect, useState} from 'react';
import cx from 'classnames';

import CurrentTrackInfo from './components/CurrentTrackInfo';
import Controls from './components/Controls';

import AppContext from '../../appstate/AppContext';

import './player.scss';

export default function Player() {
  const {
    player: {currentUrl}
  } = useContext(AppContext);

  const visible = !!currentUrl;
  const [animationStep, setAnimationStep] = useState<string>(visible ? '' : 'hidden');

  useEffect(() => {
    if (visible) {
      setAnimationStep('showing');
      setTimeout(() => setAnimationStep(''), 200);
    } else {
      setAnimationStep('hiding');
      setTimeout(() => setAnimationStep('hidden'), 200);
    }
  }, [visible]);

  return (
    <footer className={cx('player', animationStep)}>
      <div className="player-content">
        <CurrentTrackInfo />
        <Controls />
        <div />
      </div>
    </footer>
  );
}
