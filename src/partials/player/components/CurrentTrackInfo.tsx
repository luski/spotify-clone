import React, {useContext} from 'react';
import {Link} from 'react-router-dom';

import AppContext from '../../../appstate/AppContext';

export default function CurrentTrackInfo() {
  const {
    player: {playlist, currentTrack}
  } = useContext(AppContext);

  if (!playlist) {
    return <div className="current-track" />;
  }
  return (
    <div className="current-track">
      <img src={playlist.image} alt="" />
      <div className="info">
        <Link to={`/${playlist.type}/${playlist.id}`}>
          <div className="title txt bold">{playlist.title}</div>
          <div className="subtitle txt">{currentTrack?.name}</div>
        </Link>
      </div>
    </div>
  );
}
