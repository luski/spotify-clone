import React, {useContext} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import cx from 'classnames';

import AppContext from '../../../appstate/AppContext';
import {formatDuration} from '../../../utils';

const STEP = 3; //3s

export default function Controls() {
  const {
    player: {play, pause, playing, elapsedTime, getDuration, setElapsedTime, previousTrack, nextTrack}
  } = useContext(AppContext);

  let duration = getDuration();
  if (isNaN(duration)) {
    duration = 0;
  }
  const percentProgress = !isNaN(duration) && duration !== 0 ? (elapsedTime * 100) / duration : 0;

  return (
    <div className="controls">
      <div className="actions">
        <button>
          <FontAwesomeIcon icon="undo" onClick={() => setElapsedTime(Math.max(0, elapsedTime - STEP))} />
        </button>
        <button onClick={previousTrack}>
          <FontAwesomeIcon icon="step-backward" />
        </button>
        <button className={cx('main', {playing})} onClick={() => (playing ? pause() : play())}>
          <FontAwesomeIcon icon={playing ? 'pause' : 'play'} />
        </button>
        <button onClick={nextTrack}>
          <FontAwesomeIcon icon="step-forward" />
        </button>
        <button>
          <FontAwesomeIcon icon="redo" onClick={() => setElapsedTime(Math.min(duration, elapsedTime + STEP))} />
        </button>
      </div>
      <div className="progress">
        <span className="time">{formatDuration(elapsedTime * 1000)}</span>
        <span className="progress-line">
          <span className="inner-line" style={{width: `${percentProgress}%`}} />
        </span>
        <span className="time">{formatDuration(duration * 1000)}</span>
      </div>
    </div>
  );
}
