import React, {useContext, useState, useCallback} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {useHistory, useLocation} from 'react-router-dom';

import SearchInput from '../../components/searchInput/SearchInput';
import Popup from '../../components/popup/Popup';
import AppContext from '../../appstate/AppContext';

import './header.scss';

const Header = () => {
  const {logout, me} = useContext(AppContext);
  const [searchPhrase, setSearchPhrase] = useState('');
  const history = useHistory();
  const location = useLocation();

  const handlePhraseChange = useCallback(
    (phrase: string) => {
      setSearchPhrase(phrase);
      const url = `/search?q=${phrase}`;
      if (location.pathname.startsWith('/search')) {
        history.replace(url);
      } else {
        history.push(url);
      }
    },
    [history, location]
  );

  return (
    <header className="header">
      <SearchInput value={searchPhrase} onChange={e => handlePhraseChange(e.target.value)} />
      <Popup
        handlerContent={({open}) => (
          <div className="profile-popup-handler">
            {me?.image && <img src={me.image} alt="" />}
            <div>{me?.name}</div>
            <FontAwesomeIcon icon={open ? 'chevron-up' : 'chevron-down'} />
          </div>
        )}
        items={[
          {label: 'Account', onClick: () => alert('TODO')},
          {label: 'Settings', onClick: () => alert('TODO')},
          {label: 'Log Out', onClick: logout}
        ]}
      />
    </header>
  );
};

export default Header;
