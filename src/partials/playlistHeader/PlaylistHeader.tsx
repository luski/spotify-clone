import React from 'react';

import './playlistHeader.scss';

type Props = {
  title: string;
  description: string;
  imageUrl: string;
  type: 'album' | 'playlist';
};

export default ({imageUrl, title, description, type}: Props) => (
  <div className="playlist-header">
    <img src={imageUrl} alt="" />
    <div className="texts">
      <div className="type">{type}</div>
      <div className="title">{title}</div>
      <div
        className="description"
        dangerouslySetInnerHTML={{
          __html: description
        }}
      />
    </div>
  </div>
);
