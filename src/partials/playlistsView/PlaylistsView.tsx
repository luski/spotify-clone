import React from 'react';
import {Link} from 'react-router-dom';

import {Playlist} from '../../api/types';

import './playlistsView.scss';

type Props = {
  playlists: Playlist[];
};

type TileProps = {
  playlist: Playlist;
};

const Tile = ({playlist: {id, image, title, subtitle, type}}: TileProps) => {
  return (
    <Link to={`/${type}/${id}`} className="playlist-tile tile" title={title}>
      <img src={image} alt="" className="thumbnail" />
      <div className="title">
        <div className="name txt bold">{title}</div>
        <div
          className="subtitle txt"
          dangerouslySetInnerHTML={{
            __html: subtitle || ''
          }}
        />
      </div>
    </Link>
  );
};

const PlaylistsView = ({playlists}: Props) => {
  return (
    <ul className="tiles">
      {playlists.map(playlist => (
        <li key={playlist.id}>
          <Tile playlist={playlist} />
        </li>
      ))}
    </ul>
  );
};

export default PlaylistsView;
