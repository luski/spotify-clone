import React from 'react';
import {NavLink} from 'react-router-dom';

import './navigation.scss';

export default function Navigation() {
  return (
    <nav className="app-navigation">
      <ul>
        <li>
          <NavLink
            to="/"
            exact
            className="txt bold"
            isActive={(match, {pathname}) =>
              pathname === '/' || pathname.startsWith('/categories/') || pathname.startsWith('/playlist/')
            }>
            Categories
          </NavLink>
        </li>
        <li>
          <NavLink
            to="/new-releases"
            className="txt bold"
            isActive={(match, {pathname}) => pathname.startsWith('/new-releases') || pathname.startsWith('/album/')}>
            New Releases
          </NavLink>
        </li>
      </ul>
    </nav>
  );
}
