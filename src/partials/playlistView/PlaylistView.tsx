import React, {useContext} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import cx from 'classnames';

import './playlistView.scss';
import AppContext from '../../appstate/AppContext';
import {Playlist, PlaylistTrack} from '../../api/types';

type Props = {
  tracks: PlaylistTrack[];
  playlist: Playlist;
};

export default function PlaylistView({tracks, playlist}: Props) {
  const {
    player: {pause, play, playAnother, playing, currentUrl}
  } = useContext(AppContext);

  const handlePlayPauseClick = (trackUrl: string) => {
    if (!trackUrl) {
      return;
    }
    if (currentUrl !== trackUrl) {
      playAnother(trackUrl, tracks, playlist);
    } else if (playing) {
      pause();
    } else {
      play();
    }
  };

  return (
    <div className="playlist-view">
      <table>
        <thead>
          <tr>
            <th className="action">#</th>
            <th>Title</th>
            <th>Author</th>
            <th>
              <FontAwesomeIcon icon="clock" color="gray" size="lg" />
            </th>
          </tr>
        </thead>
        <tbody>
          {tracks.map(({id, name, author, duration, url}, i) => (
            <tr key={id} className={cx({active: url === currentUrl, disabled: !url})}>
              <td className="action" onClick={() => handlePlayPauseClick(url)}>
                <FontAwesomeIcon icon={url === currentUrl && playing ? 'pause' : 'play'} size="xs" />
                <span className="order">{i + 1}</span>
              </td>
              <td>{name}</td>
              <td>{author}</td>
              <td>{duration}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}
