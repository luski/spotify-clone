export const playlists = [{
  id: '1',
  name: 'Piseneczka',
  author: 'Łona',
  duration: '5:22',
}, {
  id: '2',
  name: 'Misie',
  author: 'Webber',
  duration: '1:23',
}, {
  id: '3',
  name: 'Szła dzieweczka do laseczka',
  author: 'Kriss Stylus',
  duration: '3:36',
}, {
  id: '4',
  name: '5 and more',
  author: 'Dj BOBO',
  duration: '2:55',
}];
