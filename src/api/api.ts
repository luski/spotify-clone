import api from './apiInstance';
import {Category, User, Playlist, PlaylistTrack} from './types';
import {formatDuration} from '../utils';

export function setAccessToken(token: string) {
  api.setAccessToken(token);
}
export async function fetchUser(): Promise<User> {
  const {display_name, images} = await api.getMe();
  const name = display_name || 'Me';
  const image = images && images.length ? images[0].url : null;
  return {name, image};
}

export async function fetchCategories(): Promise<Category[]> {
  const {
    categories: {items}
  } = await api.getCategories({limit: 35});

  return items.map(({id, icons, name}) => ({
    id,
    image: icons[0]?.url || '',
    name
  }));
}

export async function fetchCategoryPlaylists(categoryId: string): Promise<Playlist[]> {
  const {
    playlists: {items}
  } = await api.getCategoryPlaylists(categoryId);
  return items.map(({id, name, images, ...rest}) => ({
    id,
    title: name,
    subtitle: (rest as any).description,
    image: images[0]?.url || '',
    type: 'playlist'
  }));
}

export async function fetchNewReleases(): Promise<Playlist[]> {
  const {
    albums: {items}
  } = await api.getNewReleases({country: 'PL'});

  return (items as SpotifyApi.AlbumObjectFull[]).map(({id, name, images, artists}) => ({
    id,
    title: name,
    subtitle: artists[0]?.name || '',
    image: images[0]?.url || '',
    type: 'album'
  }));
}

export async function fetchPlaylist(playlistId: string, type: 'album' | 'playlist'): Promise<Playlist> {
  switch (type) {
    case 'playlist': {
      const {id, name, description, images} = await api.getPlaylist(playlistId);
      return {id, title: name, subtitle: description, image: images[0]?.url || '', type};
    }
    case 'album': {
      const {id, name, artists, images} = await api.getAlbum(playlistId);
      return {id, title: name, subtitle: artists.map(({name}) => name).join(', '), image: images[0]?.url || '', type};
    }
  }
}

export async function fetchPlaylistTracks(playlistId: string, type: 'album' | 'playlist'): Promise<PlaylistTrack[]> {
  let trackApiObjects;
  switch (type) {
    case 'playlist': {
      const {items} = await api.getPlaylistTracks(playlistId);
      trackApiObjects = items
        .map(({track}) => track)
        .filter(Boolean)
        .filter(({preview_url}) => !!preview_url);
      break;
    }
    case 'album': {
      const {items} = await api.getAlbumTracks(playlistId);
      trackApiObjects = items.filter(({preview_url}) => !!preview_url);
      break;
    }
  }
  return trackApiObjects.map(({id, name, artists, duration_ms, preview_url}) => ({
    id,
    name,
    author: artists.map(artist => artist.name).join(', '),
    duration: formatDuration(duration_ms),
    url: preview_url
  }));
}
