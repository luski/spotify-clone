export type LoginResult = {
  type: string;
  token: string;
  expiresIn: number;
};

export default function login(): Promise<LoginResult> {
  return new Promise(resolve => {
    const url = getLoginURL(['user-read-private']);
    const listener = ({data}: any) => {
      const hash = JSON.parse(data);
      if (hash.type === 'access_token') {
        window.removeEventListener('message', listener);
        resolve(hash);
      }
    };
    window.addEventListener('message', listener, false);

    const width = 800;
    const height = 800;
    // eslint-disable-next-line no-restricted-globals
    const left = screen.width / 2 - width / 2;
    // eslint-disable-next-line no-restricted-globals
    const top = screen.height / 2 - height / 2;

    window.open(
      url,
      'Spotify',
      `menubar=no,location=no,resizable=no,scrollbars=no,status=no, width=${width}, height=${height}, top=${top}, left=${left}`
    );
  });
}

const URL = 'https://accounts.spotify.com/authorize';

function getLoginURL(scopes: string[]) {
  const params = new URLSearchParams();
  params.set('client_id', process.env.REACT_APP_SPOTIFY_CLIENT_ID || '');
  params.set('redirect_uri', 'http://localhost:3000/signin-callback');
  params.set('scope', scopes.join(' '));
  params.set('response_type', 'token');

  return `${URL}?${params}`;
}
