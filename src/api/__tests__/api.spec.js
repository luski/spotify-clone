import {fetchCategories, fetchCategoryPlaylists, fetchNewReleases, fetchUser} from '../api';
import apiMock from '../apiInstance';

jest.mock('../apiInstance');

describe('API methods', () => {
  describe('fetchUser', () => {
    it('should provide user data', async () => {
      apiMock.getMe.mockImplementation(() =>
        Promise.resolve({
          display_name: 'Test User',
          images: [{url: 'avatar_url'}]
        })
      );
      const user = await fetchUser();
      expect(user).toEqual({name: 'Test User', image: 'avatar_url'});
    });
  });
  describe('fetchCategories', () => {
    it('should provide categories', async () => {
      apiMock.getCategories.mockImplementation(() =>
        Promise.resolve({
          categories: {
            items: [
              {id: 'cat1', icons: [{url: 'icon-url'}], name: 'category 1'},
              {id: 'cat2', icons: [], name: 'category 2'}
            ]
          }
        })
      );

      const categories = await fetchCategories();
      expect(categories).toEqual([
        {id: 'cat1', image: 'icon-url', name: 'category 1'},
        {id: 'cat2', image: '', name: 'category 2'}
      ]);
    });
  });
  describe('fetchCategoryPlaylists', () => {
    it('should provide category playlists', async () => {
      apiMock.getCategoryPlaylists.mockImplementation(() =>
        Promise.resolve({
          playlists: {
            items: [
              {id: 'pl1', images: [{url: 'icon-url'}], name: 'playlist 1', description: 'pl1 description'},
              {id: 'pl2', images: [], name: 'playlist 2', description: 'pl2 description'}
            ]
          }
        })
      );

      const playlists = await fetchCategoryPlaylists('cat1');
      expect(playlists).toEqual([
        {id: 'pl1', image: 'icon-url', title: 'playlist 1', subtitle: 'pl1 description', type: 'playlist'},
        {id: 'pl2', image: '', title: 'playlist 2', subtitle: 'pl2 description', type: 'playlist'}
      ]);
    });
  });
  describe('fetchNewReleases', () => {
    it('should provide new releases for country = PL', async () => {
      apiMock.getNewReleases.mockImplementation(() =>
        Promise.resolve({
          albums: {
            items: [
              {id: 'album1', images: [{url: 'icon-url'}], name: 'album 1', artists: [{name: 'artist'}]},
              {id: 'album2', images: [], name: 'album 2', artists: []}
            ]
          }
        })
      );

      const playlists = await fetchNewReleases();
      expect(playlists).toEqual([
        {id: 'album1', image: 'icon-url', title: 'album 1', subtitle: 'artist', type: 'album'},
        {id: 'album2', image: '', title: 'album 2', subtitle: '', type: 'album'}
      ]);
      expect(apiMock.getNewReleases).toHaveBeenCalledWith({country: 'PL'});
    });
  });
});
