export type User = {
  name: string;
  image: string | null;
};

export type Category = {
  id: string;
  image: string;
  name: string;
};

export type Playlist = {
  id: string;
  image: string;
  title: string;
  subtitle?: string | null;
  type: 'playlist' | 'album';
};

export type PlaylistTrack = {
  id: string;
  name: string;
  author: string;
  duration: string;
  url: string;
};
