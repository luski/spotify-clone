import React, {useContext, useEffect, useMemo, useState} from 'react';
import {useParams} from 'react-router-dom';

import AppContext from '../../appstate/AppContext';
import PlaylistsView from '../../partials/playlistsView/PlaylistsView';
import Spinner from '../../components/spinner/Spinner';
import {fetchCategoryPlaylists} from '../../api/api';
import {Category, Playlist} from '../../api/types';

import './categoryPage.scss';

export default function CategoryPage() {
  const {id} = useParams<{id: string}>();
  const [loading, setLoading] = useState(true);
  const {loggedIn, categories} = useContext(AppContext);
  const [playlists, setPlaylists] = useState<Playlist[]>([]);
  const category = useMemo<Category | undefined>(() => categories.find(c => c.id === id), [id, categories]);

  useEffect(() => {
    if (!loggedIn) {
      return;
    }
    fetchCategoryPlaylists(id)
      .then(setPlaylists)
      .then(() => setLoading(false))
      .catch(e => {
        console.error(e);
        setLoading(false);
      });
  }, [id, loggedIn]);

  if (loading) {
    return <Spinner />;
  }

  return (
    <div className="category-page">
      <h1 className="page-title">{category?.name}</h1>
      <PlaylistsView playlists={playlists} />
    </div>
  );
}
