import React, {useContext} from 'react';
import {Route, Switch} from 'react-router-dom';

import AppContext from '../appstate/AppContext';

import Header from '../partials/header/Header';
import Navigation from '../partials/navigation/Navigation';

import CategoriesPage from './categories/CategoriesPage';
import CategoryPage from './category/CategoryPage';
import PlaylistPage from './playlist/PlaylistPage';
import SearchResultPage from './SearchResultPage';
import NewReleasesPage from './newReleases/NewReleasesPage';
import LoginPage from './loginPage/LoginPage';
import Player from '../partials/player/Player';

const AppUI = () => {
  const {loggedIn} = useContext(AppContext);

  if (!loggedIn) {
    return <LoginPage />;
  }

  return (
    <div className="app-wrapper">
      <Header />
      <div className="content">
        <aside className="sidebar">
          <Navigation />
        </aside>
        <main className="page-wrapper">
          <Switch>
            <Route path="/" exact>
              <CategoriesPage />
            </Route>
            <Route path="/categories/:id">
              <CategoryPage />
            </Route>
            <Route path="/search">
              <SearchResultPage />
            </Route>
            <Route path="/new-releases" exact>
              <NewReleasesPage />
            </Route>
            <Route path="/playlist/:id">
              <PlaylistPage type="playlist" />
            </Route>
            <Route path="/album/:id">
              <PlaylistPage type="album" />
            </Route>
          </Switch>
        </main>
      </div>
      <Player />
    </div>
  );
};

export default AppUI;
