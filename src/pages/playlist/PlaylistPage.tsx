import {useParams} from 'react-router-dom';
import React, {useContext, useEffect, useState} from 'react';

import AppContext from '../../appstate/AppContext';
import {fetchPlaylist, fetchPlaylistTracks} from '../../api/api';
import Spinner from '../../components/spinner/Spinner';
import PlaylistView from '../../partials/playlistView/PlaylistView';
import PlaylistHeader from '../../partials/playlistHeader/PlaylistHeader';

import './playlistPage.scss';
import {Playlist, PlaylistTrack} from '../../api/types';

type Props = {
  type: 'album' | 'playlist';
};

export default function PlaylistPage({type}: Props) {
  const {id} = useParams<{id: string}>();
  const [loading, setLoading] = useState(true);
  const {loggedIn} = useContext(AppContext);
  const [playlist, setPlaylist] = useState<Playlist | null>(null);
  const [tracks, setTracks] = useState<PlaylistTrack[]>([]);

  useEffect(() => {
    if (!loggedIn) {
      return;
    }
    Promise.all([fetchPlaylistTracks(id, type), fetchPlaylist(id, type)]).then(([tracks, playlist]) => {
      setTracks(tracks);
      setPlaylist(playlist);
      setLoading(false);
    });
  }, [id, loggedIn, type]);

  if (loading) {
    return <Spinner />;
  }
  return (
    <div className="playlist-page">
      {playlist && (
        <>
          <PlaylistHeader
            title={playlist.title}
            description={playlist.subtitle || ''}
            imageUrl={playlist.image || ''}
            type="playlist"
          />
          <PlaylistView playlist={playlist} tracks={tracks} />
        </>
      )}
    </div>
  );
}
