import {useEffect} from 'react';
import {useLocation} from 'react-router-dom';

const SignInCallbackPage = () => {
  const {hash} = useLocation();
  useEffect(() => {
    const params = new URLSearchParams(hash.substring(1));
    window.opener.postMessage(
      JSON.stringify({
        type: 'access_token',
        token: params.get('access_token'),
        expiresIn: params.get('expires_in') || 0
      }),
      '*'
    );
    window.close();
  }, [hash]);
  return null;
};

export default SignInCallbackPage;
