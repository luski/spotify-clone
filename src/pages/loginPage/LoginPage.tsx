import React, {useContext} from 'react';

import AppContext from '../../appstate/AppContext';
import Spinner from '../../components/spinner/Spinner';

import './loginPage.scss';

export default function LoginPage() {
  const {initializing, login} = useContext(AppContext);
  return (
    <div className="login-page">
      {initializing ? (
        <Spinner />
      ) : (
        <>
          <h1>Spotify Clone</h1>
          <button className="start-button" onClick={login}>{`Let's start!`}</button>
        </>
      )}
    </div>
  );
}
