import React from 'react';
import {MemoryRouter, withRouter} from 'react-router-dom';
import {fireEvent, render} from '@testing-library/react';

import AppContext from '../../appstate/AppContext';
import CategoriesPage from '../categories/CategoriesPage';

const CATEGORIES = [...Array(30).keys()].map(id => ({
  id: `id-cat-${id}`,
  image: `img-${id}`,
  name: `Category ${id}`
}));

describe('categories page', () => {
  it('should display categories tiles', () => {
    const {getByTestId} = render(
      <MemoryRouter>
        <AppContext.Provider value={{categories: CATEGORIES}}>
          <CategoriesPage />
        </AppContext.Provider>
      </MemoryRouter>
    );
    const ul = getByTestId('tiles');
    expect(ul.children).toHaveLength(CATEGORIES.length);
    CATEGORIES.forEach((category, i) => {
      expect(ul.children[i]).toHaveTextContent(CATEGORIES[i].name);
    });
  });
  it('should redirect to category playlists page when clicked on the category tile', () => {
    const LocationDisplay = withRouter(({location}) => <div data-testid="location-display">{location.pathname}</div>);

    const {getByTestId} = render(
      <AppContext.Provider value={{categories: CATEGORIES}}>
        <LocationDisplay />
        <CategoriesPage />
      </AppContext.Provider>,
      {wrapper: MemoryRouter}
    );
    const ul = getByTestId('tiles');
    const firstTile = ul.children[0].querySelector('a');

    expect(getByTestId('location-display')).toHaveTextContent('/');
    fireEvent.click(firstTile);
    expect(getByTestId('location-display')).toHaveTextContent('/categories/id-cat-0');
  });
});
