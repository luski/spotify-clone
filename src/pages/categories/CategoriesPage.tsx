import React, {useContext} from 'react';
import {Link} from 'react-router-dom';

import AppContext from '../../appstate/AppContext';
import {Category} from '../../api/types';

import './categoriesPage.scss';

type CategoryTileProps = {
  category: Category;
};
const CategoryTile = ({category: {id, image, name}}: CategoryTileProps) => (
  <Link
    to={`/categories/${id}`}
    style={{backgroundImage: image.length ? `url(${image})` : ''}}
    className="tile category-tile">
    {name}
  </Link>
);

export default function CategoriesPage() {
  const {categories} = useContext(AppContext);
  return (
    <div className="categories-page">
      <h1 className="page-title">Browse</h1>
      <ul className="tiles" data-testid="tiles">
        {categories.map(category => (
          <li key={category.id}>
            <CategoryTile category={category} />
          </li>
        ))}
      </ul>
    </div>
  );
}
