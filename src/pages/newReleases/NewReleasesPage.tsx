import React, {useContext, useEffect, useState} from 'react';

import AppContext from '../../appstate/AppContext';
import PlaylistsView from '../../partials/playlistsView/PlaylistsView';

import Spinner from '../../components/spinner/Spinner';
import {fetchNewReleases} from '../../api/api';
import {Playlist} from '../../api/types';

import './newReleases.scss';

export default function NewReleasesPage() {
  const [loading, setLoading] = useState(true);
  const {loggedIn} = useContext(AppContext);
  const [albums, setAlbums] = useState<Playlist[]>([]);

  useEffect(() => {
    if (!loggedIn) {
      return;
    }
    fetchNewReleases()
      .then(albums => {
        setAlbums(albums);
        setLoading(false);
      })
      .catch(e => {
        console.error(e);
        setLoading(false);
      });
  }, [loggedIn]);

  if (loading) {
    return <Spinner />;
  }
  return (
    <div className="new-releases-page">
      <h1 className="page-title">Albums</h1>
      <PlaylistsView playlists={albums} />
    </div>
  );
}
