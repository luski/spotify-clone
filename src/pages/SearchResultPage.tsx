import React from 'react';
import {useLocation} from 'react-router-dom';

export default function SearchResultPage() {
  const {search} = useLocation();
  const params = new URLSearchParams(search.substring(1));
  return (
    <div>
      <h1 className="page-title">
        Search results for "<span style={{fontStyle: 'italic'}}>{params.get('q')}</span>" ... TODO
      </h1>
    </div>
  );
}
